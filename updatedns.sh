#!/bin/bash

set -e
set -o pipefail

export PATH=/home/rodrigo/.local/bin:$PATH

if test "$1"
then
	. "$1"
fi

hostname=${hostname:-$(hostname -s)}
domain=${domain:-$(hostname -d)}
nameserver=${nameserver:-$(dig +short $domain NS | grep -v '^;;' | head -n1)}

for ipv in 4 6
do
	case "$ipv" in
		4)
			type=A
			filter=global;;
		6)
			type=AAAA
			filter=global-unicast;;
	esac

	my_addresses=$(
		set +o pipefail
		ip -"$ipv" address show |
			grep -vw 'deprecated' |
			grep -o 'inet6\? [^ ]*' |
			cut -d ' ' -f 2 |
			cut -d / -f 1 |
			ipv6calc -I ipv"$ipv"addr -O ipv"$ipv"addr -E "$filter" |
			sort -u
	)

	dns_addresses=$(
		set +o pipefail
		dig +short "$hostname.$domain" @"$nameserver" "$type" |
			ipv6calc -I ipv"$ipv"addr -O ipv"$ipv"addr |
			sort -u
	)

	addresses_remove=$(
		for address in $dns_addresses
		do
			echo "$my_addresses" | grep -Fxq "$address" && continue
			echo "$address"
		done
	)

	addresses_add=$(
		for address in $my_addresses
		do
			echo "$dns_addresses" | grep -Fxq "$address" && continue
			echo "$address"
		done
	)

	test "$addresses_remove" -o "$addresses_add" || continue

	if ! test "$domain_id"
	then
		domain_id=$(
			linode-cli --suppress-warnings --text domains list |
				awk "\$2 == \"$domain\" {print \$1}"
		)
	fi

	for address in $addresses_remove
	do
		echo Removing $address from DNS.

		record_ids=$(
			linode-cli --suppress-warnings --text domains records-list "$domain_id" |
				awk "\$2 == \"$type\" && \$3 == \"$hostname\" && \$4 == \"$address\" { print \$1 }"
		)

		for record_id in $record_ids
		do
			linode-cli --suppress-warnings --text domains records-delete "$domain_id" "$record_id"
		done
	done

	for address in $addresses_add
	do
		echo Adding $address to DNS.

		linode-cli --suppress-warnings --text domains records-create --ttl_sec 300 --name "$hostname" --type "$type" --target "$address" "$domain_id"
	done
done
